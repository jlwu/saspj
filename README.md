SASPJ
===

## Introduction 

- 用來建立資料分析專案資料夾的程式
- 開發時參考SAS的文件，因此名稱就已SAS為主
- 希望能夠擴展至所有平台的資料分析專案使用


## Verstions

* 1.x 以bash shell為基礎開發
* 2.x 以MS Windows batch script
* 3.x 以Java GUI為主

## System requirement

### MS Windows
* bash shell environment for 1.x
* JRE 1.8.x or above

### Linux 
* bash shell environment for 1.x
* JRE 1.8.x or above
* 2.x not support

### MacOS
* bash shell environment for 1.x
* JRE 1.8.x or above
* 2.x not support

## Feature
